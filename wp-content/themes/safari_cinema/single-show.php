<?php

the_post();
get_header();
$fields = get_fields();
$numbers = [
		'1' => 'ב’',
		'2' => 'ג’',
		'3' => 'ד’',
		'4' => 'ה’',
		'5' => 'ו’',
		'6' => 'ש’',
		'7' => 'א’'
];
?>

<div class="top-image-page">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="top-title"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<article class="page-body post-body">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
			<div class="row">
				<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-between align-items-stretch mt-5">
			<div class="<?= ($fields['show_timetable'] || $fields['show_tel']) ? 'col-lg-6 col-12' : 'col-12'; ?> content-show-col">
				<?php if ($fields['show_desc']) : ?>
					<h2 class="block-title-about"><?= $fields['show_desc']; ?></h2>
				<?php endif;
				if ($fields['show_director']) : ?>
					<h3 class="show-base-black-title"><?= 'שם הבמאי:'.$fields['show_director']; ?></h3>
				<?php endif;
				if ($fields['show_tickets']) : ?>
					<div class="tickets-wrap">
						<?php foreach ($fields['show_tickets'] as $ticket) : ?>
							<div class="ticket-item"><?= $ticket['show_ticket_text']; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['show_director']) : ?>
					<h3 class="show-base-black-title"><?= 'שם הבמאי:'.$fields['show_director']; ?></h3>
				<?php endif;
				if ($fields['show_actors']) : ?>
					<h3 class="show-base-black-title">שם השחקנים:</h3>
					<div class="show-base-black-title font-weight-normal">
						<?= $fields['show_actors']; ?>
					</div>
				<?php endif;
				if ($fields['show_info']) : ?>
					<div class="contact-info-title"><?= $fields['show_info']; ?></div>
				<?php endif; ?>
			</div>
			<?php if ($fields['show_timetable'] || $fields['show_tel']) : ?>
			<div class="col-lg-6 col-12">
				<?php if ($fields['show_timetable']) : ?>
					<div class="table-responsive table-show">
						<table class="table">
							<thead>
							<tr>
								<th scope="col">
									תאריך
								</th>
								<th scope="col">
									שעה
								</th>
								<th scope="col">
									יום
								</th>
								<th scope="col">
									מקום
								</th>
								<th scope="col">
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($fields['show_timetable'] as $x => $row) : ?>
								<tr>
									<td class="name-col">
										<?= $row['date']; ?>
									</td>
									<td>
										<?= $row['hour']; ?>
									</td>
									<td>
										<?php $timestamp = strtotime($row['date']);
										$day = date('N', $timestamp);
										echo $numbers[$day];
										?>
									</td>
									<td>
										<?= $row['place']; ?>
									</td>
									<td>
										<a href="<?= (isset($row['url']) && $row['url']) ? $row['url'] : ''; ?>"
										   class="table-link">
											לרכישה
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				<?php endif;
				if ($fields['show_tel']) : ?>
					<div class="tel-row-show">
						<span class="show-base-black-title">רכישה טלפונית: </span>
						<a href="tel:<?= $fields['show_tel']; ?>" class="show-base-black-title show-phone">
							<?= $fields['show_tel']; ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php if ($fields['show_video_link']) {
	get_template_part('/views/partials/content', 'video', [
		'video' => $fields['show_video_link'],
	]);
}
if ($fields['show_gallery']) {
	get_template_part('/views/partials/content', 'gallery', [
		'gallery' => $fields['show_gallery'],
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'show_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 2,
	'post_type' => 'show',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'show_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_shows']) {
	$samePosts = $fields['same_shows'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 2,
		'orderby' => 'rand',
		'post_type' => 'show',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="home-shows-output mt-5 mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-page-output mb-4">
						<?= $fields['same_text'] ? $fields['same_text'] : '<h2>הצגות נוספות</h2>'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php foreach ($samePosts as $x => $show) {
					get_template_part('/views/partials/card', 'show', [
						'post' => $show,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
get_footer(); ?>


