<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block mb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args = array(
			's' => $s
		);
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) { ?>
		<h4 class="base-title-red my-3"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center">
			<?php  while ( $the_query->have_posts() ) { $the_query->the_post(); $hey = 1; $type = get_post_type();
				$link = get_the_permalink(); ?>
				<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-show-col">
					<div class="post-show-item"
						<?php if (has_post_thumbnail()) : ?>
							style="background-image: url('<?= postThumb(); ?>')"
						<?php endif;?>>
						<div class="post-show-item-image">
							<div class="post-show-item-content">
								<div class="d-flex flex-column justify-content-start align-items-center flex-grow-1">
									<a class="show-item-title" href="<?= $link; ?>"><?php the_title(); ?></a>
									<?php if ($type === 'show') : ?>
									<h3 class="post-show-date">
										<?php echo 'יום ’'.$date = get_field('show_date', get_the_ID()); ?>
									</h3>
									<?php endif; ?>
								</div>
								<a href="<?= $link; ?>" class="show-item-link">
									<?= ($type === 'show') ? 'לרכישה' : 'קרא עוד'; ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="text-center pt-5">
					<h4 class="base-block-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
