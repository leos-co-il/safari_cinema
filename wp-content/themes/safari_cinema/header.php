<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>

<?php
$tel = opt('tel');
$file = opt('header_file');
?>
<header>
    <div class="container-fluid header-top">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-3 col-sm-6 col-7 logo-col">
				<a href="/" class="logo">
					<img src="<?= opt('logo')['url'] ?>" alt="logo">
				</a>
            </div>
            <div class="col-xl-6 col-md-7 col-1">
            	<nav id="MainNav" class="h-100">
                    <div id="MobNavBtn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
                </nav>
            </div>
			<?php if ($tel || $file) : ?>
				<div class="col-xl-3 col-md-2 col-sm-5 col-4 header-contacts-col">
					<div class="row align-items-stretch justify-content-end header-contacts-row">
						<?php if ($tel) : ?>
							<div class="col-xl-auto mb-xl-0 mb-1 col-12 d-flex align-items-center justify-content-end tel-head-col">
								<a href="tel:<?= $tel; ?>" class="header-tel header-tel-link d-flex justify-content-center align-items-center">
									<span class="tel-text"><?= $tel; ?></span>
									<img src="<?= ICONS ?>form-tel.png" class="header-tel-icon">
								</a>
							</div>
						<?php endif;
						if ($file = opt('header_file')) : ?>
							<div class="col-xl-auto col-12 d-flex align-items-center justify-content-end tel-head-col">
								<a href="<?= $file['url']; ?>" class="header-tel header-file-link d-flex justify-content-center align-items-center" download>
									<span class="pdf-text">חוברת מנויים</span>
									<img src="<?= ICONS ?>pdf.png" class="pdf-icon">
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
        </div>
    </div>
	<?php if ($messages = opt('messages')) : ?>
		<div class="header-bottom">
			<div class="top-events main-ticker-wrap">
				<div class="msg-ticker-wrap" style="animation-duration: 10s;">
					<?php foreach ($messages as $msg): ?>
						<div class="msg-item">
							<a href="<?= $msg['msg_link']; ?>" class="event-link" style="direction: rtl;">
									<span class="base-large-text white-text font-weight-normal">
                                        <?= $msg['msg_title'] ?>
                                    </span>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="form-wrapper-base">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png">
						</span>
						<?php if ($f_title = opt('pop_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('6'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pop-search">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-search">
					<span class="close-search">
							<img src="<?= ICONS ?>close.png">
						</span>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="triggers-wrap">
	<div class="trigger-wrap-item search-trigger">
		<img src="<?= ICONS ?>search.png" alt="search-trigger">
		<span>חפשו</span>
	</div>
	<div class="trigger-wrap-item pop-trigger">
		<img src="<?= ICONS ?>form-mail.png" alt="pop-trigger">
		<span>צרו
קשר</span>
	</div>
</div>
