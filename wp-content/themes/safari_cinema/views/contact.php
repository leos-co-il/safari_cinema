<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$post = opt('post_address');

?>


<div class="top-image-page" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="top-title"><?php single_post_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<article class="page-body mb-5">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-12">
				<?php if ($tel) : ?>
					<div class="contact-item contact-item-link wow zoomIn" data-wow-delay="0.2s">
						<h3 class="contact-info-title">
							חייגו אלינו
						</h3>
						<a href="tel:<?= $tel; ?>" class="contact-info">
							<?= $tel; ?>
						</a>
					</div>
				<?php endif;
				if ($fax) : ?>
					<div class="contact-item wow zoomIn" data-wow-delay="0.3s">
						<h3 class="contact-info-title">
							פקססו אלינו
						</h3>
						<div class="contact-info">
							<?= $fax; ?>
						</div>
					</div>
				<?php endif;
				if ($mail) : ?>
					<div class="contact-item wow zoomIn" data-wow-delay="0.4s">
						<h3 class="contact-info-title">
							כיתבו לנו
						</h3>
						<a href="mailto:<?= $mail; ?>" class="contact-info">
							<?= $mail; ?>
						</a>
					</div>
				<?php endif;
				if ($post) : ?>
					<div class="contact-item wow zoomIn" data-wow-delay="0.5s">
						<h3 class="contact-info-title">
							דואר
						</h3>
						<div class="contact-info">
							<?= $post; ?>
						</div>
					</div>
				<?php endif;
				if ($open_hours) : ?>
					<div class="contact-item wow zoomIn" data-wow-delay="0.6s">
						<h3 class="contact-info-title">
							השעות שלנו
						</h3>
						<div class="contact-info">
							<?= $open_hours; ?>
						</div>
					</div>
				<?php endif;
				if ($address) : ?>
					<div class="contact-item wow zoomIn" data-wow-delay="0.7s">
						<h3 class="contact-info-title">
							בקרו אותנו
						</h3>
						<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="contact-info">
							<?= $address; ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-sm-6 col-12 mt-sm-0 mt-4">
				<div class="form-wrapper-base">
					<?php if ($fields['contact_form_title']) : ?>
						<h2 class="pop-form-title"><?= $fields['contact_form_title']; ?></h2>
					<?php endif;
					getForm('6'); ?>
				</div>
				<?php if ($map = opt('map_image')) : ?>
					<a href="<?= $map['url']; ?>" class="w-100 d-flex my-3" data-lightbox="map">
						<img src="<?= $map['url']; ?>" alt="map" class="w-100">
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
