<?php if (isset($args['video']) && $args['video']) : ?>
	<section class="video-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="video-wrapper">
						<div class="post-video">
							<img src="<?= getYoutubeThumb($args['video']); ?>">
							<span class="play-video" data-video="<?= getYoutubeId($args['video']); ?>">
						</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
