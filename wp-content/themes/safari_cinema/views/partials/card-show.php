<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-show-col">
		<div class="post-show-item more-card" data-id="<?= $args['post']->ID; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')"
		<?php endif;?>>
			<div class="post-show-item-image">
				<div class="post-show-item-content">
					<div class="d-flex flex-column justify-content-start align-items-center flex-grow-1">
						<a class="show-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
						<h3 class="post-show-date">
							<?php echo 'יום ’'.$date = get_field('show_date', $args['post']->ID); ?>
						</h3>
					</div>
					<a href="<?= $link; ?>" class="show-item-link">
						לרכישה
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
