<?php if (isset($args['gallery']) && $args['gallery']) : ?>
<div class="gallery-all">
	<div class="gallery-slider" dir="rtl">
		<?php foreach ($args['gallery'] as $img) : ?>
			<a class="gallery-item" href="<?= $img['url']; ?>" style="background-image:
				url('<?= $img['url']; ?>')" data-lightbox="image">
			</a>
		<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>
