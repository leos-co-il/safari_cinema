<?php if (isset($args['faq'])) : ?>
	<section class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="block-page-output">
							<?= (isset($args['block_title']) && $args['block_title']) ? '<h2>'.$args['block_title'].'</h2>' :
								'<h2>שאלתם אותנו, אנחנו ענינו</h2>'; ?>
						<p>
							<?= (isset($args['block_title']) && $args['block_title']) ? $args['block_title'] : ''; ?>
						</p>
					</div>
				</div>
				<div class="col-12">
					<div class="row align-items-stretch justify-content-center">
						<div class="col-lg-6 col-12 d-flex align-items-center">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<?php foreach ($args['faq'] as $num => $item) : ?>
									<li class="nav-item">
										<a class="nav-link <?= ( $num === 0) ? 'active' : ''; ?>" id="<?= $num; ?>-tab" data-toggle="tab" href="#faq-<?= $num; ?>" role="tab"
										   aria-controls="<?= $num; ?>" aria-selected="true">
											<?= $item['faq_question']; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div class="col-lg-6 col-12">
							<div class="tab-content">
								<?php foreach ($args['faq'] as $num => $item) : ?>
									<div class="tab-pane fade <?= ( $num === 0) ? 'show active' : ''; ?>" id="faq-<?= $num; ?>" role="tabpanel" aria-labelledby="<?= $num; ?>-tab">
										<div class="base-output"><?= $item['faq_answer']; ?></div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
