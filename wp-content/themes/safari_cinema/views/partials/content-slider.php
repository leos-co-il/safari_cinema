<?php if (isset($args['content']) && $args['content']) : ?>
	<section class="slider-base-wrap arrows-slider">
		<div class="container slider-container">
			<div class="row justify-content-center align-items-center slider-row">
				<div class="<?= (isset($args['img']) && $args['img']) ? 'col-xl-6 col-lg-7': 'col-lg-12'; ?> col-12
				 slider-wrap-col">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div>
									<div class="base-output"><?= $content['content']; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php if (isset($args['img']) && $args['img']) : ?>
					<div class="col-xl-6 col-lg-5 col-12 d-flex justify-content-center align-items-center slider-img-col">
						<div class="slider-img-part">
							<img src="<?= $args['img']['url']; ?>" class="slider-img">
							<div class="slider-red-part"></div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
