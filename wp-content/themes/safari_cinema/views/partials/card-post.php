<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-6 col-12 cat-show-col">
		<div class="category-item-wrap more-card" data-id="<?= $args['post']->ID; ?>">
			<div class="d-flex justify-content-center align-items-stretch flex-grow-1 flex-wrap">
				<a class="col-sm-4 col-12 category-name-wrap" href="<?= $link; ?>">
					<?= $args['post']->post_title;  ?>
				</a>
				<div class="col-sm-8 col-12 category-text-wrap">
					<p class="base-text mb-0">
						<?= text_preview($args['post']->post_content, '12')?>
					</p>
				</div>
			</div>
			<a class="category-image" <?php if(has_post_thumbnail($args['post'])): ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?> href="<?= $link; ?>">
			</a>
		</div>
	</div>
<?php endif; ?>
