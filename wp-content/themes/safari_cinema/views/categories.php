<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$terms = get_terms( [
		'taxonomy' => 'show_cat',
		'hide_empty' => false,
] );
?>
<article class="base-page-body">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="block-page-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="row">
			<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
			</div>
		</div>
		<?php endif;
		if ($terms) : ?>
			<div class="row justify-content-start align-items-stretch">
				<?php foreach ($terms as $x => $term) : $link = get_term_link($term); ?>
					<div class="col-lg-6 col-12 cat-show-col wow fadeIn" data-wow-delay="0.<?= $x; ?>s">
						<div class="category-item-wrap">
							<div class="d-flex justify-content-center align-items-stretch flex-grow-1 flex-wrap">
								<a class="col-sm-4 col-12 category-name-wrap" href="<?= $link; ?>">
									<?= $term->name; ?>
								</a>
								<div class="col-sm-8 col-12 category-text-wrap">
									<p class="base-text mb-0">
										<?= text_preview(category_description($term->term_id), '12')?>
									</p>
								</div>
							</div>
							<div class="category-image" <?php if ($img_cat = get_field('cat_img', $term)): ?>
								style="background-image: url('<?= $img_cat['url']; ?>')"
							<?php endif; ?>>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<div class="col post-show-col">
					<div class="cat-form-item">
						<?php if ($fields['cats_form_title']) : ?>
							<h3 class="cat-form-title"><?= $fields['cats_form_title']; ?></h3>
						<?php endif;
						if ($fields['cats_form_subtitle']) : ?>
							<h4 class="cat-form-subtitle cats_form_subtitle"><?= $fields['cats_form_subtitle']; ?></h4>
						<?php endif;
						getForm('11'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['cats_chosen']) : ?>
	<section class="home-shows-output mt-5 mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-page-output mb-4">
						<?= $fields['cats_text'] ? $fields['cats_text'] : '<h2>הצגות חמות </h2>'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php foreach ($fields['cats_chosen'] as $x => $show) {
					get_template_part('/views/partials/card', 'show', [
							'post' => $show,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
get_footer(); ?>
