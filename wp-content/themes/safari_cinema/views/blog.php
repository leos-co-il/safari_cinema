<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = get_posts([
		'numberposts' => 8,
		'post_type' => 'post',
		'suppress_filters' => false
]);
$posts_all = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
		'suppress_filters' => false
]);
?>
<div class="top-image-page" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="top-title"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<article class="page-body">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="row">
			<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
			</div>
		</div>
		<?php endif;
		if ($posts) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts as $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 8) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-posts show-item-link home-cats-link add-more-link" data-type="post"
						 data-tax-type="category">
						טען עוד מאמרים
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item'])  {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>

