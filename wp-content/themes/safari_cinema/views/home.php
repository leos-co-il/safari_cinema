<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main" <?php if ($fields['main_img']) : ?>
	style="background-image: url('<?= $fields['main_img']['url']; ?>')"
<?php endif; ?>>
	<?php if ($fields['home_title_small'] || $fields['home_title_big']) : ?>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h3 class="home-small-title">
						<?= $fields['home_title_small']; ?>
					</h3>
					<h2 class="home-big-title"><?= $fields['home_title_big']; ?></h2>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php if ($fields['home_cats_show']) : ?>
	<section class="home-cats">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-sm-11 col-12">
					<div class="cats-row-home">
						<?php foreach ($fields['home_cats_show'] as $k => $cat) : ?>
							<a class="home-cat-item wow fadeInUp" href="<?= get_term_link($cat); ?>"
							data-wow-delay="0.<?= $k * 2; ?>s">
								<span class="title-cat-wrap">
									<?= $cat->name; ?>
								</span>
								<span class="cat-item-home-img" <?php if ($img = get_field('cat_img', $cat)) : ?>
									style="background-image: url('<?= $img['url']; ?>')"
								<?php endif; ?>></span>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($fields['home_cats_show_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['home_cats_show_link']['url']; ?>" class="show-item-link home-cats-link">
							<?= (isset($fields['home_cats_show_link']['title']) && $fields['home_cats_show_link']['title']) ?
									$fields['home_cats_show_link']['title'] : 'לכל הקטגוריות'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<section class="home-form-base">
	<img src="<?= IMG ?>form-left.png" alt="mask" class="mask-img mask-left">
	<img src="<?= IMG ?>form-right.png" alt="mask" class="mask-img mask-right">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-9 col-12">
				<div class="row justify-content-center align-items-center mb-3">
					<?php if ($fields['home_form_title_1']) : ?>
						<div class="col-auto">
							<h2 class="form-title-big"><?= $fields['home_form_title_1']; ?></h2>
						</div>
					<?php endif;
					if ($fields['home_form_subtitle_1']) : ?>
						<div class="col-auto">
							<h3 class="form-title-small"><?= $fields['home_form_subtitle_1']; ?></h3>
						</div>
					<?php endif; ?>
				</div>
				<?php getForm('7'); ?>
			</div>
		</div>
	</div>
</section>
<div class="container">
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
<?php if ($fields['home_shows']) : ?>
	<section class="home-shows-output">
		<div class="container">
			<div class="row">
				<?php foreach ($fields['home_shows'] as $x => $show) {
					get_template_part('/views/partials/card', 'show', [
							'post' => $show,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_banner']) : ?>
	<section class="home-banner">
		<div class="container">
			<div class="row">
				<div class="col-12 wow zoomIn" data-wow-delay="0.2s">
					<img src="<?= $fields['home_banner']['url']; ?>" alt="banner" class="w-100">
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
} ?>
<section class="form-about-section">
	<img src="<?= IMG ?>stage-left.png" alt="mask" class="stage-img stage-left">
	<img src="<?= IMG ?>stage-right.png" alt="mask" class="stage-img stage-right">
	<div class="home-form-base mb-0">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-9 col-12">
					<div class="row justify-content-center align-items-center mb-3">
						<?php if ($fields['home_form_title_2']) : ?>
							<div class="col-auto">
								<h2 class="form-title-big"><?= $fields['home_form_title_2']; ?></h2>
							</div>
						<?php endif;
						if ($fields['home_form_subtitle_2']) : ?>
							<div class="col-auto">
								<h2 class="form-title-small"><?= $fields['home_form_subtitle_2']; ?></h2>
							</div>
						<?php endif; ?>
					</div>
					<?php getForm('7'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="about-home-block">
		<?php if ($fields['home_about_title']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto mb-3">
						<h2 class="block-title-about"><?= $fields['home_about_title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container about-home-wrapper">
			<div class="row justify-content-center align-items-center">
				<div class="<?= $fields['home_about_img'] ? 'col-lg-7' : 'col-xl-12'; ?> col-12">
					<div class="base-output">
						<?= $fields['home_about_text']; ?>
					</div>
					<?php if ($fields['home_about_link']) : ?>
						<div class="row justify-content-start mb-3">
							<div class="col-auto">
								<a href="<?= $fields['home_about_link']['url']; ?>" class="about-link">
									<?= (isset($fields['home_about_link']['title']) && $fields['home_about_link']['title']) ?
											$fields['home_about_link']['title'] : 'עוד עלינו'; ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['home_about_img']) : ?>
					<div class="col-lg-5 col-12 about-img-col">
						<div class="about-image">
							<img src="<?= $fields['home_about_img']['url']; ?>" alt="about-img">
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php
if ($fields['faq_item'])  {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_desc' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
