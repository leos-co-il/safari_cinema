<?php
$names = ['youtube', 'facebook', 'instagram', 'twitter'];
$socials = get_social_links($names);
$facebook = opt('facebook');
$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$post = opt('post_address');
?>

<div class="sticky-form" id="fix-form">
	<div class="container">
		<div class="row justify-content-center align-items-center mb-3">
			<?php if ($foo_title = opt('foo_form_title')) : ?>
				<div class="col-auto">
					<h2 class="foo-form-title"><?= $foo_title; ?></h2>
				</div>
			<?php endif;
			if ($foo_subtitle = opt('foo_form_subtitle')) : ?>
				<div class="col-auto">
					<h3 class="foo-form-subtitle"><?= $foo_subtitle; ?></h3>
				</div>
			<?php endif; ?>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-10 col-11">
				<?php getForm('8'); ?>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
		</a>
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-md col-6 foo-menu main-footer-menu">
					<h3 class="foo-title">ניווט</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2', '',
								'main_menu h-100 text-right'); ?>
					</div>
				</div>
				<div class="col-md col-6 foo-menu links-footer-menu">
					<h3 class="foo-title">קישורים</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2'); ?>
					</div>
				</div>
				<div class="col-md col-sm-6 col-12 foo-menu contacts-footer-menu">
					<h3 class="foo-title">פרטי התקשרות</h3>
					<div class="menu-border-top contact-menu-foo">
						<ul class="contact-foo-list">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										<?= $tel; ?>
									</a>
								</li>
							<?php endif;
							if ($fax) : ?>
								<li>
								<span class="contact-info">
								<?= $fax; ?>
							</span>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info">
										<?= $mail; ?>
									</a>
								</li>
							<?php endif;
							if ($post) : ?>
								<li>
								<span class="contact-info">
								<?= $post; ?>
							</span>
								</li>
							<?php endif;
							if ($open_hours) : ?>
								<li>
								<span class="contact-info">
									<?= $open_hours; ?>
								</span>
								</li>
							<?php endif;
							if ($address) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="contact-info">
										<?= $address; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-6 col-12 foo-menu">
					<h3 class="form-title-big mb-3 text-right">צרו איתנו קשר</h3>
					<?php getForm('9'); ?>
				</div>
			</div>
			<?php if ($socials) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="socials-footer">
							<?php foreach ($socials as $social_link) : if ($social_link['url']) : ?>
								<a href="<?= $social_link['url']; ?>" target="_blank" class="social-link">
									<i class="fab fa-<?php echo $social_link['name'];
									echo ($social_link['name'] === 'facebook') ? '-f' : '';?>">
									</i>
								</a>
							<?php endif; endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
