<?php
get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = get_posts([
	'numberposts' => 12,
	'post_type' => 'show',
	'tax_query' => array(
		array(
			'taxonomy' => 'show_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'show',
	'tax_query' => array(
		array(
			'taxonomy' => 'show_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
<div class="top-image-page" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="top-title"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<article class="page-body">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="row">
			<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
			</div>
		</div>
		<?php endif;
		if ($posts) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts as $post) {
					get_template_part('views/partials/card', 'show', [
						'post' => $post,
					]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 12) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-posts show-item-link home-cats-link add-more-link" data-type="show"
						 data-tax-type="show_cat" data-term="<?= $query->term_id; ?>">
						טען עוד הצגות
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
if ($content = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $content,
		'img' => get_field('slider_img', $query),
	]);
}
if ($faq = get_field('faq_item', $query))  {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => get_field('faq_title', $query),
			'block_desc' => get_field('faq_text', $query),
			'faq' => $faq,
		]);
}
get_footer(); ?>

