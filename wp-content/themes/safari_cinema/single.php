<?php

the_post();
get_header();
$fields = get_fields();
?>


<article class="page-body post-body">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="row">
			<div class="col-auto col-lg-11 col-12 breadcrumbs-custom align-self-start">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
			</div>
		</div>
		<?php endif; ?>
		<div class="row justify-content-between align-items-stretch mt-5">
			<div class="col-lg-7 col-12">
				<div class="base-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-12 fixed-form">
				<div class="post-form-wrap ">
					<?php if ($title_f = opt('post_form_title')) : ?>
						<h2 class="post-form-title-big"><?= $title_f; ?></h2>
					<?php endif;
					if ($subtitle_f = opt('post_form_subtitle')) : ?>
						<h2 class="post-form-title-small"><?= $subtitle_f; ?></h2>
					<?php endif; ?>
					<?php getForm('10'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['post_video_link']) {
	get_template_part('/views/partials/content', 'video', [
			'video' => $fields['post_video_link'],
	]);
}
if ($fields['post_gallery']) {
	get_template_part('/views/partials/content', 'gallery', [
			'gallery' => $fields['post_gallery'],
	]);
}
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 2,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 2,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="home-shows-output mt-5 mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-page-output mb-4">
						<?= $fields['same_posts_title'] ? $fields['same_posts_title'] : '<h2>מאמרים דומים</h2>'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php foreach ($samePosts as $x => $show) {
					get_template_part('/views/partials/card', 'post', [
						'post' => $show,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>


